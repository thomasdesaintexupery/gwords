# Welcome to Good words

This site is a repository of good words I stumbled onto.

- [Citations en français](/01-fr)
- [Quotes in english](/02-en)

!!! tip

    Use **search** on top bar to look for words everywhere.
