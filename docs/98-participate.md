# Participate

_Do you want to add a good word? It's really simple._

1. Go to the page, and click the pen icon :material-pencil-outline:
2. \_You'll need to log or create a Gitlab account
3. Add the citation with this template

```markdown
> My good words
>
> -- <cite>Author, [source](link-to-source), YYYY-MM-DD</cite>
```

Submit a merge-request and you're done.
